
resource "aws_instance" "admin-server-production" {
  ami           = data.aws_ami.ubuntu.id
  instance_type = "t2.large"
  key_name = "server-keypair"
  tags = {
    Name = "admin-server-production"
  }
  subnet_id = module.vpc.public_subnets[0]
  depends_on = [
    module.vpc
  ]
}

resource "aws_elb" "server-elb-production" {
  name               = "foobar-terraform-elb"
  availability_zones = module.vpc.azs

  access_logs {
    bucket        = "foo"
    bucket_prefix = "bar"
    interval      = 60
  }

  listener {
    instance_port     = 8000
    instance_protocol = "http"
    lb_port           = 80
    lb_protocol       = "http"
  }

  listener {
    instance_port      = 8000
    instance_protocol  = "http"
    lb_port            = 443
    lb_protocol        = "https"
  }

  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 3
    target              = "HTTP:8000/"
    interval            = 30
  }

  instances                   = [aws_instance.admin-server-production.id]
  cross_zone_load_balancing   = true
  idle_timeout                = 400
  connection_draining         = true
  connection_draining_timeout = 400

  tags = {
    Name = "foobar-terraform-elb"
  }
}
